# Purpose

After facing the same re-configuration tasks again and again, I put this together to automate a basic lockdown - primarily for new cloud server builds based on provider / distro ISO's, and regular audits of existing systems. I've found this useful as a regular re-runnable on servers every few months, to check if any new potential footholds need closing off.

There's a lot of default options on a standard Debian Buster build that need to be modified - all fairly repetitive actions. Lynis isn't the be-all and end-all of audits, but it's a great starting point, so this script is designed to make the process a little less repetitive. As new security issues and updates come out the script can be updated to be able to enhance configurations and used packages - it should also be re-unnable on the same server too.

Now Debian 10 runs on pretty much all 64-bit - inc. ARM - this script helps manage a basic level of security across all sorts of shapes of server and device. IoT's general lack of security springs to mind. I recommend ensuring you set up rsyslog to pipe logs over TLS to a centralised server, and from which you can then apply appropriate monitoring of outputs from guardians such as arpwatch.

I strongly recommend you backup your SSH configs before running this script[^4], because if you've currently using legacy or lower security configurations, your access may be blocked once the service restarts with the enhanced configuration.

[^4]: Issue #1 added to cover taking backups automatically on each run.

## Warning
Although this script should get you to a Lynis hardening index of 85, it is no substitute for the hardening protocols you likely already have. This script merely automates a lot of the bulk repetitive changes.

I've tested this on a VM running Debian 10, which was easy to rollback to a default install state (plus default / unconfigured installs of SSH, Apache, Shorewall and PHP); but that is no guarantee that this is defect free. Use at your own risk.

## Focus
Used to set up Debian 10 after a fresh install with some default configurations. Optionally deals with:

- Apache 2
- Php
- Ssh server
- ~~Iptables (or any netfilter firewall which may leave unused rules)~~[^2]
- blocking usb storage at module level
- Blocking usb storage as this is generally just a potential foothold on servers [^1]

[^1]: This is different for ARM devices that may only have a USB storage device as a hard drive. Pi users please take note!
[^2]: Need to review this - not sure it's a good idea to mess with the firewall config when there's no stats to backup "unused" rule definitions yet

These core areas cover the majority of all installs e.g. api server, web server, admin box etc. Each node is optional by parameter:

```bash
# look at normal areas plus ssh and apache; plus a run of lynis, using a file called `custom.prf` in the current folder
sudo ./secure-autoconfig.sh ssh apache runlynis

# default - no optional modules will be enhanced
sudo /secure-autoconfig.sh

# all but a final run of Lynis when complete
sudo ./secure-autoconfig.sh most
```

### Non-Optional Areas of Focus
Some of the things the script configures are not optional and they are:
- Blocking firewire as servers do not need it
- Harden UMASK in login.defs
- Install a number of packages that require no config, but are support packages for dpkg verification etc
- Restrict access to any compilers found on the system
- Harden default sysctl settings
- Install various audit tools
    - rkhunter
    - tripwire
    - auditd
    - arpwatch
    - sysstat
    - ...

## Parameters

| **Option** | enable single language | harden apache | harden php | harden ssh | block usb storage | run lynis |
| ---- | ---- |  ---- | ---- | ---- | ---- | ---- |
| **Parameter** | **singlang** / omitted | **apache** / omitted | **php** / omitted | **ssh** / omitted | **usb**[^3] / omitted | **runlynis** / omitted |

[^3]: I'd recommend not applying the usb option as you're likely to be using USB storage for HD or other storage, critical to the operation of your node.

For ease of access additional parameter options **all** and **most** can be used, which the script expands into:

| **all** | **most** | **pi** (or any arm64 board) |
| ---- | ---- | ---- |
| every single option is applied, then Lynis is run | every single option is applied, but Lynis is _not_ run | As per **most** but less USB |

## Pi & arm64 Boards
The latter expander, **pi**, excludes blocking USB storage for safety and does not run Lynis. As the ARM boards are lower spec it may be more useful to run the script, tweak and adjust some things and then run lynis seperately. Alternatively you could use the following to do all except USB storage blocking:

```bash
sudo ./secure-autoconfig.sh singlang apache php ssh runlynis
```

## Use with Lynis
These configs are the kinds of things that Lynis finds on initial scan, although some Lynis tests
are skipped as it's up to the machine owner to determine purpose. This saves a lot of time when building
both new servers, and consistent images for things like cloud base images. Deals with the repetitive multiple configurations.

I had to spend some time considering applying settings for things like password age, as on-site systems use MFA inc. Yubikey as challenge authentication. However that's not really practical (or possible) in cases such as data-centre hosted servers; therefore the password configuration is strengthened.

## Basic custom.prf
I generally exclude the following tests from a generic build, then determining whether or not to review any based on the intended
usage for that server:
- NETW-2705
- BOOT-5122 
- STRG-1840 
- NAME-4028 
- LOGG-2190 
- BANN-7126 
- BANN-7130 
- TOOL-5002 
- FILE-6310 
- AUTH-9286

I've included a base [template](custom.prf) in this repo.

## Firewall Conflict
I've noticed that the default configuration for some netfilter firewalls e.g. Shorewall specifically enables martian packet logging:
```conf
net     NET_IF          dhcp,tcpflags,logmartians,nosmurfs,sourceroute=0,physical=
```

One of the Lynis checks is KRNL-6000, which deals with enhanced sysctl configurations. I'm fine with the firewall logging this packet type for other reasons, even though I've built the sysctl hardening portion of the script to adhere to the Lynis audit rules. That means if you either change your firewall configuration to *not* log martians, or use a firewall that does not do this you'll be in compliance by default.

I've added the exception for this sysctl value to the `skip-test=KRNL-6000:<sysctl value` list in [the custom profile supplied](custom.prf).

# Recommended Use
- After running this script reboot and run lynis; develop a custom.prf specific to your needs
- Review each item in the audit and assess appropriately based on your custom profile
- Consider installing acct, auditd, tripwire, arpwatch, rkhunter and ensuring you have appropriate monitoring for each

## References
Credit goes to various online resources who've provided information to base my decisions in this script on, although the majority of the controls applied were just me getting bored of the repetition :smile:

- [nachoparker](https://ownyourbits.com/2017/12/23/security-audit-your-arm-board-with-lynis/)
- [Karim's Blog](https://elatov.github.io/2017/06/install-lynis-and-fix-some-suggestions/)
- Lynis Community [documentation](https://cisofy.com/lynis/controls/) is a little sparse on clues in some areas (that's why you pay for enterprise), but still a great starting-point.
- [Hal Pomeranz](http://www.deer-run.com/~hal/linux_passwords_pam.html)
- [GNU acct](https://www.gnu.org/software/acct/manual/accounting.html)
- Useful [processing accounting](https://www.ostechnix.com/monitor-user-activity-linux/) quick reference
