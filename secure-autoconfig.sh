#!/bin/bash
# Use this script at your own risk, provided as-is and without warranty or garuntee
# Used to set up Debian 10 after a fresh install with some default configurations.

# Recommendation:
# - After running this script reboot and run lynis; develop a custom.prf specific to your needs
# - Review each item in the audit and assess appropriately based on your custom profile
# - Consider installing acct, auditd, tripwire, arpwatch, rkhunter and ensuring you have appropriate monitoring for each

## Defaults - for ease of adjustment I've left them at the top for tweaking
passMaxDays=365
passMinDays=1
passWarnAge=14
passMinLength=20
default="yes"
hardenApache="no"
hardenSsh="no"
hardenUsb="no"
hardenPhp="no"
singleLanguage="no"
runLynis="no"

## Some formatting to make it a bit more readable
function fbN() {
	echo "\e[1m\e[92m$1\e[0m"
}

function fbF() {
	echo "\e[1m\e[91m$1\e[0m"
}

function fbR() {
	echo "\e[1m\e[93m$1\e[0m"
}

function fbV() {
	echo "\e[1m\e[96m$1\e[0m"
}

function fbI() {
	echo "\e[1m\e[94m$1\e[0m"
}

function pNote() {
	printf "\n$(fbN "NOTE: ")$1"
}

function pFail() {
	printf "\n$(fbF "FAIL: ")$1"
}

function pRec() {
	printf "\n$(fbR "RECOMMEND: ")$1"
}

function pVerify() {
	printf "\n$(fbV "VERIFY: ")$1"
}

function pImportant() {
	printf "\n$(fbI "IMPORTANT: ")"
	tput bold
	echo "$1"
	tput sgr0
}

## Args parsing
# multiply out most / all if present
[[ -n "$@" ]] && args=$( echo "$@" | tr '[:upper:]' '[:lower:]')
[[ "$args" == "all" ]] && args="php apache ssh singlang usb runlynis" 
[[ "$args" == "most" ]] && args="php apache ssh singlang usb"
[[ "$args" == "pi" ]] && args="php apache ssh singlang" 

# then deal with the args provided
for arg in $args
do
	case $arg in
		apache)
			# check if apache is installed - if it is assume harden unless overriden by arg
			apachePath=$(whereis apache2 | awk '{print $2}')
			if [ -f "$apachePath" ]; then
				hardenApache=${2:-$default}
			fi
			;;
		php)
			# check if php is installed - if it is assume harden unless overriden by arg. Expect multiple .ini files
			pNote "Looking for PHP configs on this system. Please wait"
			phpIniPath=$(sudo find / -name php.ini 2>&1 | grep ^/etc/ | sort)
			buffer=""
			for iniFile in $phpIniPath
			do
				if [ -f "$iniFile" ]; then
					buffer="$buffer $iniFile"
				fi
			done
			phpIniPath=$buffer
			[ -n "$phpIniPath" ] && hardenPhp=$default # hardenPhp=${3:-$default} || hardenPhp="no"
			;;
		ssh)
			# check if ssh is installed - if it is assume harden unless overriden by arg
			sshPath=$(whereis ssh | awk '{print $2}')
			if [ -f "$sshPath" ]; then
				#hardenSsh=${4:-$default}
				hardenSsh=$default
			fi
			;;
		singlang)
			#singleLanguage=${1:-$default}
			singleLanguage=$default
			;;
		usb)
			hardenUsb=$default
			;;
		runlynis)
			# has to be explicitly enabled as initial script runs may want to focus on results / errors, before moving onto detailed Lynis audit
			#runLynis=${5:-"no"}
			runLynis=$default
			;;
	esac
done

## install lynis, add lynis community repo to apt sources
function setupLynis() {
	# reduce download impact by selecting only single language package updatesAUTH-9308
	if [ ! -f /etc/apt/apt.conf.d/99disable-translations ] && [ $singleLanguage == "yes" ]; then
		echo 'Acquire::Languages "none";' | sudo tee /etc/apt/apt.conf.d/99disable-translations
	else
		pNote "Not disabling translations for APT"
	fi

	# find out if the apt https package is already installed
	[ -n "$(dpkg-query -l apt-transport-https | grep ^ii)" ] && pNote "Apt HTTPS Installed. Skipping step" || pNote "Installing APT HTTPs..."; sudo apt-get install apt-transport-https -y  2>&1 > /dev/null

	# # if not already done, add the Lynis community repository and associated key
	if [ ! "$(grep packages.cisofy.com -R /etc/apt/)" ]; then
		# check if this is an amd64 compatible machine
		machine=$(uname -m)
		case "$machine" in
			aarch64|arm64|armel|armhf)
				# for debian on arm e.g. Pi 3>, arm64 is the repo arch model 
				arch="[arch=arm64]"
				;;
			x86_64)
				arch="[arch=amd64]"
				;;
			*)
				arch=""
				;;
		esac

		# get the key, apply the source and do an update
		wget -q -O - https://packages.cisofy.com/keys/cisofy-software-public.key | sudo apt-key add - 2>&1 && pNote "Public key for cisofy repo installed" || pFail "Could not get Lynis repo key. Run apt-get update for more detials"
		echo "deb $arch https://packages.cisofy.com/community/lynis/deb/ stable main" | sudo tee /etc/apt/sources.list.d/cisofy-lynis.list 2>&1 > /dev/null && pNote "Source added"

		sudo apt-get update 2>&1 > /dev/null && pNote "Packages updated ok" || pFail "Could not update packages. Check apt logs"
	else
		pNote "Not adding cisofy community"
	fi

	if [ "$(dpkg-query -l lynis 2>&1 | grep ^ii)" ]; then
		pNote "Ugrading Lynis to latest community version. Please wait."
		sudo apt-get upgrade lynis -y 2>&1 > /dev/null || pFail "Could not upgrade lynis. Check apt logs"
	else
		pNote "Installing community version of Lynis. Please wait."
		sudo apt-get install lynis -y 2>&1 > /dev/null || pFail "Could not install lynis. Check apt logs"
	fi

	pRec "Run lynis to get a complete set of suggestions and recommendations for this specific system."
	pRec "Create a custom set of skip-test items and use: lynis audit system --profile custom.prf"
}

## lock down SSH
function replaceSshOption() {
	# params: sshd_config path, settingName, settingNew
	test=$(grep "$2" $1)
	pNote "SSH - Checking $2..."
	if [[ $test == \#* ]]; then
		# deal with commented out variants - enable if no value specified, otherwise set new value and enable
		[ -z "$3" ] && sed -i "s|\#$2.*|$2|" $1 || sed -i "s|\#$2.*|$2 $3|" $1
	elif [[ $test != \#* ]]; then
		# deal with already enabled variants - disable if no value specified, otherwise set new value and leave enabled
		[ -z "$3" ] && sed -i "s|$2.*|\#$2|" $1 || sed -i "s|$2.*|$2 $3|" $1
	else
		# setting didn't previously exist
		echo "$2 $3" >> $1
	fi
}

function enhanceSsh() {
	if [ "$hardenSsh" == "no" ]; then
		pNote "Skipping SSH... "
		return 1 
	fi
	
	# find sshd_config path
	sshConfig=/etc/ssh/sshd_config
	if [ ! -f "$sshConfig" ]; then
		sshConfig=$(sudo find / -name sshd_config 2>&1 | grep ^/etc/ | sort | head -1)
	fi

	if [ -z "$sshConfig" ] || [ ! -f "$sshConfig" ]; then
		pFail "Cannot find sshd_config but we know it's installed. Exiting..."
		exit 1
	else
		pNote "Found SSH config..."
	fi

	# to ensure this is a re-runnable script, check for the comment line
	comment="## Review following replacements for kex, cipher and hmac settings on your ssh server (try ssh-audit):"
	sshConfigUpdated=$(grep "$comment" $sshConfig)
	if [ -z "$sshConfigUpdated" ]; then
		# add (commented out) Kex, HMAC and Cipher combinations and notify user that they should review
		# printf "\n\n$comment\n" >> $sshConfig
		# echo "# KexAlgorithms curve25519-sha256@libssh.org" >> $sshConfig
		# echo "# Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com" >> $sshConfig
		# echo "# MACs hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com" >> $sshConfig
		cat >> $sshConfig <<- Append

		$(echo -e $comment)
		# KexAlgorithms curve25519-sha256@libssh.org
		# Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com"
		# MACs hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com
		Append
		pRec "Review ssh_config for commented out kex / cipher / mac suggestions. Run ssh-audit for completeness"
	else
		pNote "SSH Config already updated with Kex / Cipher / MAC suggestions"
	fi
	
	# if not installed, install ssh-audit and suggest to user - maybe run it and show output?
	[ -n "$(dpkg-query -l ssh-audit 2>&1 | grep ^ii)" ] && pNote "ssh-audit already installed" || sudo apt-get install ssh-audit -y 2>&1 > /dev/null

	# more complex SSH config: application of hardened config on basis of:
	# - if setting does not exist, add it
	# - if setting does exist but is commented, replace the line
	# - if setting exists, replace the line
	# These updates are all based on the default configs, so if you've changed them it may leave the settings as-is
	replaceSshOption $sshConfig "AllowTcpForwarding" "no"
	replaceSshOption $sshConfig "ClientAliveCountMax" "2"
	replaceSshOption $sshConfig "Compression" "no"
	replaceSshOption $sshConfig "LogLevel" "VERBOSE"
	replaceSshOption $sshConfig "MaxAuthTries" "2"
	replaceSshOption $sshConfig "MaxSessions" "2"
	replaceSshOption $sshConfig "PermitRootLogin" "no"
	replaceSshOption $sshConfig "TCPKeepAlive" "no"
	replaceSshOption $sshConfig "X11Forwarding" "no"
	replaceSshOption $sshConfig "AllowAgentForwarding" "no"
	replaceSshOption $sshConfig "Port" "99999" # customise to your own needs
	
	current=$(grep "#HostKey.*ed25519.*" $sshConfig)
	if [ -n "$current" ]; then
		new=${current//#}
		pNote "Enabling ed25519 SSH host key"
		sudo sed -i "s|$current|$new|" $sshConfig
	fi

	# really we shouldn't be using non-EC approaches anymore. If (tin-foil hats at the ready) the govs can do it, bet that crims can as they have more money
	current=$(grep "^HostKey.*rsa.*" $sshConfig)
	if [ -n "$current" ]; then
		pNote "Disabling RSA SSH host key"
		sudo sed -i "s|$current|#$current|" $sshConfig
	fi

	# grey area of EC but defo to avoid if there are questions re: RNG. My opinion bolstered by ssh-audit recommendations
	# and not convinced it should be used - https://www.owasp.org/index.php/Testing_for_Weak_Encryption_(OTG-CRYPST-004)
	current=$(grep "^HostKey.*ecdsa.*" $sshConfig)
	if [ -n "$current" ]; then
		pNote "Disabling ECDSA SSH host key"
		sudo sed -i "s|$current|#$current|" $sshConfig
	fi

	pRec "Change the port number (currently impossible port 99999) to your custom port preference -or- set to 22 and skip lynis test SSH-7408"
	pRec "Run 'dpkg-reconfigure openssh-server' to generate keys that were previously disabled; then restart the service"
}

## lock down firewire
function blockFirewire() {
	if [ -z "$(grep "blacklist firewire-core" /etc/modprobe.d/ -R)" ]; then
		echo "blacklist firewire-core" | sudo tee /etc/modprobe.d/firewire.conf > /dev/null 2>&1
		pRec "Firewire now blacklisted. Reboot for effect"
	else
		pNote "Firewire already blacklisted"
	fi
}

## No need for USB storage on most servers in cloud racks (but likely will on Pi's!)
function blockUsbStorage() {
	if [ "$hardenUsb" == "no" ]; then
		pNote "Skipping USB..."
		return 1
	fi

	if [ -z "$(grep "blacklist usb-storage" /etc/modprobe.d/ -R)" ]; then
		cat > /etc/modprobe.d/usb-storage.conf <<-EoC 
		# block access to usb storage for security reasons
		blacklist usb-storage
		EoC
		pRec "USB storage now blacklisted. Reboot for effect"
	else
		pNote "Already applied USB storage block"
	fi
}

## Resrict permissions within login.def and init.rc
function hardenLoginDefs() {
	# replace UMASK from default 022 to 027 if this distro uses login.defs
	loginDefs=/etc/login.defs
	if [ -f $loginDefs ]; then
		if [ -z "$(grep ^UMASK $loginDefs | grep 27)" ]; then
			sudo sed -i 's/^UMASK.*/UMASK 027/' $loginDefs 
			pNote "Enhanced UMASK in login.defs."

			# apply min and max password ages - although if you have MFA such as Yubikey tied in, this is less of an issue			
			sudo sed -i "s|^PASS_MAX_DAYS.*|PASS_MAX_DAYS $passMaxDays|" $loginDefs || pFail "Could not set max password age in $loginDefs"
			sudo sed -i "s|^PASS_MIN_DAYS.*|PASS_MIN_DAYS $passMinDays|" $loginDefs || pFail "Could not set min password age in $loginDefs"
			sudo sed -i "s|^PASS_WARN_AGE.*|PASS_WARN_AGE $passWarnAge|" $loginDefs || pFail "Could not set password warn days in $loginDefs"
			sudo sed -i "s|^#PASS_MIN_LEN*|PASS_MIN_LEN $passMinLength|" $loginDefs || pFail "Could not set password minimum length in $loginDefs"
			pNote "Enhanced password requirements in login.defs. Additional password hardening to be done later in PAM module cracklib."
		fi
	fi

	# some systems that have been upgraded many times may still have pre-systemd components, some of which
	# may be partially active. Check for existence first before enhancing.
	if [ -f /etc/init.d/rc ]; then
		if [ -z "$(grep ^UMASK /etc/init.d/rc | grep 27)" ]; then
			sudo sed -i 's/^umask.*/umask 027/' /etc/init.d/rc
			pNote "Enhanced umask in init/rc"
		fi
	fi
}

## install zero-config maintainable auditors or tools
function installZeroConfigPackages() {

	for package in apt-show-versions debsums
	do
		if [ -n "$(dpkg-query -l $package 2>&1 | grep ^ii)" ]; then
			pNote "Zero-config component $package already installed"
		else
			pNote "Installing $package..."
			sudo apt-get install $package -qy 2>&1 > /dev/null || pFail "Could not install $package. Check apt-get --fix-broken for more info"
		fi 
	done

	# TODO dig out the other package names
}

## lock down compilers
function hardenCompilers() {
	# enumerate all compilers
	for package in $(dpkg -l | egrep -i "compil" | awk '{print $2}')
	do
		#path=$(whereis "$package" | awk '{print $2}')
		path=$(sudo which "$package") # need sudo whereis seems more reliable. 
		if [ -f "$path" ]; then
			pNote "Compiler $package found as $path. Restricting to all but ug..."
			sudo chmod o-rwx $path || pFail "Could not restrict $path. Check manually"
		else
			# some packages listed as compilers in the description do not have an executable path (e.g. libs)
			pVerify "Compiler $package found but path ($path) invalid given. Skipping"
		fi
	done

	# it seems that as and cc do not show up in the dpkg query list, even with root priv. Therefore we should try 
	# and restrict both these compilers on Linux
	for package in cc as
	do
		path=$(sudo which "$package")
		if [ -f "$path" ]; then
			pNote "Additional compiler $package found as $path. Restricting to all but ug..."
			sudo chmod o-rwx $path || pFail "Could not restrict $path. Check manually"
		else
			# some packages listed as compilers in the description do not have an executable path (e.g. libs)
			pVerify "Additional compiler $package found but path ($path) invalid given. Skipping"
		fi
	done
}

## Sysctl configurations
function setupSysctl() {
	# take backup of current settings
	path=/etc/sysctl.d/90-lynis.conf

	if [ -f "$path" ]; then
		pNote "Sysctl settings already applied, skipping"
		return
	fi

	backup=sysctl-defaults-$(date +"%T").conf
	sudo sysctl -a > $backup

	# taken from the lynis.log and reproduced - adding disabling ipv6 by default
	cat  > $path <<- NewConfig
	#################################
	# Added by secure-autoconfig.sh #
	#################################
	kernel.core_uses_pid = 1
	kernel.kptr_restrict = 2
	kernel.sysrq = 0
	kernel.yama.ptrace_scope = 1 2 3
	net.ipv4.conf.all.accept_redirects = 0
	net.ipv4.conf.all.log_martians = 1
	net.ipv4.conf.all.rp_filter = 1
	net.ipv4.conf.all.send_redirects = 0
	net.ipv4.conf.default.accept_redirects = 0
	net.ipv4.conf.default.accept_source_route = 0
	net.ipv4.conf.default.log_martians = 1
	net.ipv6.conf.all.accept_redirects = 0
	net.ipv6.conf.default.accept_redirects = 0
	net.ipv6.default.disable_ipv6 = 1
	NewConfig

	pNote "Added new sysctl config at $path (original backed up to $backup)"
	pNote "Updating sysctl config. Please wait "
	sudo sysctl --system 2>&1 > /dev/null || pFail "Problem updating sysctl. Run sudo sysctl --system to reproduce and identify problem"
}

## Apache reconfiguration
function enhanceApache() {
	if [ "$hardenApache" == "no" ]; then
		pNote "Skipping Apache or Apache executable not found"
		return 1
	fi

	pNote "Looking for apache security modules..."

	modInstalled=0

	found=$(dpkg-query -l libapache2-mod-security2 2>&1 | grep ^ii)
	if [ "$found" ]; then
		pNote "Mod security already installed. Checking configuration in Apache"
	else
		pNote "Installing mod_security for Apache. Please wait."
		sudo apt-get install -qy libapache2-mod-security2 2>&1 > /dev/null || pFail "Could not install libapache2-mod-security2. Check manually"
		[ -f /etc/modsecurity/modsecurity.conf-recommended ] && sudo mv /etc/modsecurity/modsecurity.conf{-recommended,}
	fi

	config=/etc/modsecurity/modsecurity.conf
	if [ -f $config ]; then
		modified=$(grep "^SecRuleEngine On" $config)
		if [ -n "$modified" ]; then
			pNote "Updating modSec configuration"
			sudo sed -i 's/SecRuleEngine DetectionOnly/SecRuleEngine On/' $config
			# set a 1Mb (1048576 bytes) upload limit if you're uploading files, or 1 byte if you're not
			sudo sed -i 's/SecRequestBodyLimit .*/SecRequestBodyLimit 1/' $config
			# set a minimal limit for post operations - used 64k (65536 bytes) here, although this is pretty large for most scenarios
			sudo sed -i 's/SecRequestBodyNoFilesLimit .*/SecRequestBodyNoFilesLimit 65536/' $config
			sudo sed -i 's/SecRequestBodyInMemoryLimit .*/SecRequestBodyInMemoryLimit 65536/' $config
			#sudo sed -i 's/SecRequestBodyLimit.*/SecRequestBodyLimit /' $config
			sudo sed -i 's/SecResponseBodyAccess On/SecResponseBodyAccess Off/' $config
			
			modInstalled=1
		fi
	fi

	found=$(dpkg-query -l libapache2-mod-evasive 2>&1 | grep ^ii)
	if [ "$found" ]; then 
		pNote "Mod Evasive already installed."
	else
		pNote "Installing mod_evasive for Apache. Please wait."
		sudo apt-get install -qy libapache2-mod-evasive 2>&1 > /dev/null || pFail "Could not install libapache2-mod-evasive. Check manually"
	fi

	evasiveConfig=/etc/apache2/mods-enabled/evasive.conf
	if [ -f $evasiveConfig ]; then
		modified=$(grep ^DOSHashTableSize $evasiveConfig 2>&1)
		if [ -z "$modified" ]; then
			# uncomment standard items in this config - all are commented out by default
			pNote "Updating modEvasive configuration"
			sudo sed -i "s|#DOSHashTableSize.*|DOSHashTableSize 3097|" $evasiveConfig
			sudo sed -i "s|#DOSHashTableSize.*|DOSHashTableSize 3097|" $evasiveConfig
			sudo sed -i "s|#DOSPageCount.*|DOSPageCount 2|" $evasiveConfig
			sudo sed -i "s|#DOSSiteCount.*|DOSSiteCount 50|" $evasiveConfig
			sudo sed -i "s|#DOSPageInterval.*|DOSPageInterval 1|" $evasiveConfig
			sudo sed -i "s|#DOSSiteInterval.*|DOSSiteInterval 1|" $evasiveConfig
			sudo sed -i "s|#DOSBlockingPeriod.*|DOSBlockingPeriod 10|" $evasiveConfig
			pVerify "Review the mod_evasive conf - set your DOS notification email and any system command e.g. fail2ban you need to apply"
			pVerify "Test the anti-DoS with: sudo perl /usr/share/doc/libapache2-mod-evasive/examples/test.pl"

			modInstalled=1
		fi
	fi

	if [ "$modInstalled" == "1" ]; then
		pNote "Configuration(s) changed - restarting Apache"
		sudo apache2ctl restart
	fi
}

## additionally lock down access to the PHP bin, because PHP is the root of all evil
# https://www.owasp.org/index.php/PHP_Configuration_Cheat_Sheet
function applyPhpSetting() {
	# params: setting name (inc ';' if commented out by default, will be assumed uncommented in new setting), value, file
	currentSetting=$1
	newSetting=${currentSetting//;}
	newValue=$2
	file=$3

	pNote "Updating $newSetting PHP setting to $newValue for $file..." 
	# NOTE : used a different escape char ('#') here as some settings are paths w/fwd slashes. Avoids conflict with normal s/ sed pattern
	sed -i "s#$currentSetting.*=.*#$newSetting = $newValue#" $file || pFail "$newSetting not applied. Check configs"
}

function installSnuffleupagus() {
	found=$(dpkg-query -l "snuffle*" 2>&1 | grep ^ii)
	if [ ! -n "$found" ]; then
		pNote "Snuffleupagus already installed. Skipping"
		return 1
	fi

	# https://snuffleupagus.readthedocs.io/ release may need to be monitored
	# https://github.com/nbs-system/snuffleupagus/releases/tag/v0.4.1 - new version (5) has dependancy issues on Debian 10 stable
	wget -q -O snuffle.deb https://github.com/nbs-system/snuffleupagus/releases/download/v0.4.1/snuffleupagus_0.4.1_amd64.deb

	pNote "Trying install of Snuffleupagus deb. Please wait."
	sudo DEBIAN_FRONTEND=noninteractive dpkg -i snuffle.deb 2>&1 > /dev/null || pFail "Could not install Snuffleupagus deb - check dependences"
	pVerify "Refer to https://snuffleupagus.readthedocs.io/config.html for further config instructions"
}

function enhancePhp() {
	if [ "$hardenPhp" == "no" ]; then
		pNote "Skipping php or unable to find php.ini... "
		return 1
	fi

	installSnuffleupagus

	phpBinPath=$(sudo which php) 
	if [ -f "$phpBinPath" ]; then
		pNote "Restricting PHP exec to all but ug - found in package manager..."
		sudo chmod o-rwx $phpBinPath || pFail "Could not restrict $phpBinPath. Check manually"
	else
		# double check that a non-package derivative installation has been deployed
		phpVersion=$(php -r "echo PHP_MAJOR_VERSION . '.' . PHP_MINOR_VERSION;")
		phpPackage=$(dpkg -l | egrep -i "php$phpVersion" | awk '{print $2}')
		phpBinPath=$(whereis "php$phpVersion" | awk '{print $2}')
		# TODO refactor this function to de-duplicate code
		if [ -f "$phpBinPath" ]; then
			pNote "Restricting PHP exec to all but ug - found indirectly outside package manager..."
			sudo chmod o-rwx $phpBinPath || pFail "Could not restrict permissions for $phpBinPath. Check manually"
		else
			pVerify "PHP not found but asked to harden / restrict - searched for dpkg listing and direct executable 'php$Version'"
		fi
	fi

	# and then the rest... https://www.php.net/manual/en/security.php
	for iniFile in $phpIniPath
	do
		# some to turn off - inc. file uploads
		for config in allow_url_include allow_url_fopen expose_php display_errors display_startup_errors ignore_repeated_errors allow_webdav_methods file_uploads enable_dl track_errors html_errors
		do
			pNote "Updating $config PHP setting for $iniFile..." 
			sed -i "s/$config.*/$config = Off/g" $iniFile
			
			if ! grep -q "$config" $iniFile; then
				pVerify "Adding $config PHP setting to $iniFile"
				echo "$config = Off" >> $iniFile
			fi
		done

		# some to turn on
		for config in log_errors
		do
			pNote "Updating $config PHP setting for $iniFile..." 
			sed -i "s/$config.*/$config = On/g" $iniFile
			
			if ! grep -q "$config" $iniFile; then
				pVerify "Adding $config PHP setting to $iniFile"
				echo "$config = On" >> $iniFile
			fi
		done

		# arbritrary configurations - some that do exist by default
		sed -i 's/;.*Production Value: "GPCS"/variables_order = "GPCS"/' $iniFile
		sed -i 's/disable_functions.*,/&system, exec, shell_exec, passthru, phpinfo, show_source, highlight_file, popen, proc_open, fopen_with_path, dbmopen, dbase_open, putenv, move_uploaded_file, chdir, mkdir, rmdir, chmod, rename, filepro, filepro_rowcount, filepro_retrieve, posix_mkfifo/' $iniFile
		
		# other settings which follow a pattern of application
		# forces PHP config to fail
		applyPhpSetting "session.referer_check" "/configure/this/properly" $iniFile
		applyPhpSetting "memory_limit" "50M" $iniFile
		applyPhpSetting "post_max_size" "20M" $iniFile 
		applyPhpSetting "max_execution_time" "60" $iniFile
		applyPhpSetting "report_memleaks" "On" $iniFile
		printf "\n$(fbR "RECOMMEND: ")Set secure PHP-only accessible location for PHP session.save_path in $iniPath"
		printf "\n$(fbR "RECOMMEND: ")Set specific name for PHP session.name in $iniPath"
		# head /dev/urandom | tr -dc A-Za-z0-9 | head -c 16 ; echo ''
		applyPhpSetting "session.name" "RvGj9A3KDU70p6cL" $iniFile
		applyPhpSetting "session.auto_start" "0" $iniFile
		applyPhpSetting "session.use_trans_sid" "0" $iniFile
		printf "\n$(fbR "RECOMMEND: ")Set FQDN name for PHP session.cookie_domain in $iniPath"
		# forces PHP config to fail
		applyPhpSetting "session.cookie_path" "/configure/this/properly" $iniFile
		applyPhpSetting "session.use_strict_mode" "1" $iniFile
		applyPhpSetting "session.use_cookies" "1" $iniFile
		applyPhpSetting "session.use_only_cookies" "1" $iniFile
		applyPhpSetting "session.cookie_lifetime" "14400" $iniFile # 4 hours 
		applyPhpSetting ";session.cookie_secure" "1" $iniFile
		applyPhpSetting "session.cookie_httponly" "1" $iniFile
		applyPhpSetting "session.cookie_samesite" "Strict" $iniFile
		applyPhpSetting "session.cache_expire" "30" $iniFile
		applyPhpSetting "session.sid_length" "256" $iniFile
		applyPhpSetting "session.sid_bits_per_character" "6" $iniFile # PHP 7.2+
		applyPhpSetting "session.hash_function" "1" $iniFile # PHP 7.0-7.1
		applyPhpSetting "session.hash_bits_per_character" "6" $iniFile # PHP 7.0-7.1
		applyPhpSetting "extension=snuffleupagus.so" "" $iniFile
		applyPhpSetting "sp.configuration_file" "/etc/php/conf.d/snuffleupagus.rules" $iniFile
	done

	# apply the ca-certs paths
	pNote "Updating PHP with ca-certs.crt location..."
	sed -i "s|;openssl.cafile=.*|openssl.cafile=/etc/ssl/certs/ca-certificates.crt|" $phpIniPath || pFail "Couldn't update PHP with ca-certs.crt. Check locations"	

	pVerify "PHP hardening complete, but you'll need to configure specifics noted above"
}

## Add some standard audit tools, configuration can be scripted from local network sources
function addAuditTools() {
	pNote "Adding audit tools"

	# msmtp prevents exim install
	# rkhunter is excellent at watching file changes, deleted files, listening processes and many more
	# arpwatch monitors arp notifications (i.e. devices added / removed / ip-repurposed) on the network and adds to syslog
	# auditd 
	# sysstat
	# tripwire is a lockable, configurable file system watcher

	for package in rkhunter msmtp arpwatch auditd sysstat tripwire acct
	do 
		found=$(dpkg-query -l "$package" 2>&1 | grep ^ii)
		if [ ! -n "$found" ]; then
			pNote "Installing $package. Attempting to skip any direct tty input during package install - you may need to run dpkg --configure afterwards: "
			sudo DEBIAN_FRONTEND=noninteractive apt-get install -qy "$package" 2>&1 > /dev/null || pFail "Unable to successfully install $package. Run apt-get install and check output"
		else
			pVerify "$package already installed. Ensure it's properly configured"
		fi
	done

	# auditd default ruleset
	pNote "Replacing default audit.rules (usually empty anyway with a standard set"
	sudo cp ./auditd.ruleset /etc/audit/rules.d/audit.rules || pFail "Could not copy base auditd ruleset across. Check permissions or previous errors"
	# restart (even if only with the package-maintained rules) to aleviate the Lynis test results
	sudo systemctl restart auditd || pFail "Could not restart auditd - check config copied ok and journalctl -xe"

	# rkhunter specific configuration
	pNote "Enabling rkhunter on APT events"
	sudo sed -i 's|^APT_AUTOGEN=.*|APT_AUTOGEN="true"|' /etc/default/rkhunter  || pFail "Could not enable pkg manager checks for rkhunter. Check permissions or previous errors"
	
	# enable arpwatch 
	pNote "Grabbing ethernet adapater name to enable and start arpwatch service (assume systemd)."
	ethName=$(ip link | awk -F: '$0 !~ "lo|vir|wl|^[^0-9]"{print $2;getline}' | tr -d "[:blank:]")
	sudo systemctl enable arpwatch@$ethName || pFail "Could not enable service arpwatch@$ethName"
	sudo systemctl start arpwatch@$ethName || pFail "Could not start service arpwatch@$ethName"
	runCheck=$(systemctl | grep arpwatch@ | grep running | awk '{print $1}')
	if [ -n "$runCheck" ]; then
		pRec "$runCheck now running. Recommend hooking up a centralised logging system to your reporting dashboard"
	else
		pFail "Could not get the arpwatch service running. Manually attempt start and check that you have a valid ethernet adapter"
	fi

	# get sysstat operational - can be useful for post-mortem analysis of outages or attacks
	pNote "Configuring sysstat to capture every 10 mins"
	sudo sed -i 's|^ENABLED="false"|ENABLED="true"|' /etc/default/sysstat
	#sudo sed -i "s|5-55/10 * * * * root command -v debian-sa1 > /dev/null && debian-sa1 1 1|*/25 * * * * root command -v debian-sa1 > /dev/null && debian-sa1 1 1|" /etc/default/sysstat
	sudo systemctl restart sysstat 2>&1 > /dev/null || pFail "Could not restart sysstat - check config and journalctl -xe"

	# get process accounting operational
	sudo systemctl enable acct 2>&1 > /dev/null && sudo systemctl start acct 2>&1 > /dev/null || pFail "Could not enable acct service. Manual check needed in journalctl -xe"

	# final recommendations
	pRec "You will need to re-define the tripwire site and local keys, then re-init"
	pRec "Review the tool configuration for all the audit tools installed, ensure notifications are enabled for each in the way you want"
}

## restrict the cups config package to prevent redirection of prints to poacher-defined sources
function restrictCupsConf() {
	if [ -f /etc/cups/cupsd.conf ]; then
		pNote "Restricting access to print configuration"
		sudo chmod o-r /etc/cups/cupsd.conf 2>&1 > /dev/null || pFail "Unable to chmod cupsd.conf"
		pVerify "Why do you -need- printer access on a server?" 
	fi
}

function enhancePasswordSecurity() {
	# found=$(dpkg-query -l libpam-cracklib 2>&1 | grep ^ii)
	# if [ -n "$found" ]; then
	if [ -n $(dpkg-query -l libpam-cracklib 2>&1 | grep ^ii) ]; then
		pNote "Installing PAM module 'cracklib'"
		sudo apt-get install -qy libpam-cracklib 2>&1 > /dev/null || pFile "Could not download and / or install module. Manually check apt install" 
		pNote "Reconfiguring cracklib"
		crackConfig=/etc/pam.d/common-password
		old="pam_cracklib.so retry=3 minlen=8 difok=3"
		new="pam_cracklib.so retry=3 minlen=$passMinLength difok=3 ucredit=-2 lcredit=-5 dcredit=-2 ocredit=-1"
		sudo sed -i "s|$old|$new|" $crackConfig
	fi

	pRec "You may want to invest in a Yubikey or similar device for desktop and workstation scenarios, and tie in sudo access to MFA"
}

## Main
# Display running config before we start and ask for confirmation. Font adjustments are a pain in the bash: https://askubuntu.com/a/528938
pImportant "Ensure you've backed up the relevant configs!!"
 
cat <<- WarningMessage
Running configuration:
	- Harden SSH = $(echo -e "\e[4m$hardenSsh\e[0m")
	- Harden PHP = $(echo -e "\e[4m$hardnPhp\e[4m$hardenPhp\e[0m")
	- Harden Apache = $(echo -e "\e[4m$hardenApache\e[0m")
	- Block USB = $(echo -e "\e[4m$hardenUsb\e[0m")
	- Apply Sing. Lang. APT config = $(echo -e "\e[4m$singleLanguage\e[0m") 
	- Run Lynis once complete = $(echo -e "\e[4m$runLynis\e[0m")
	- Block firewire = yes
	- Restrict compilers = yes
	- Add audit tools = yes
	- Setup Lynis = yes
	- Harden sysctl = yes
	- Install zero-conf package manager support packages = yes

WarningMessage

read -p "Is this ok? (Y/y to proceed): " response

case "$response" in
	[Yy]* )
		setupLynis
		blockFirewire
		blockUsbStorage
		installZeroConfigPackages
		hardenLoginDefs
		hardenCompilers
		setupSysctl
		enhanceSsh
		enhanceApache
		enhancePhp
		restrictCupsConf
		addAuditTools
		enhancePasswordSecurity

		if [[ "$runLynis" == "yes" ]]; then
			pImportant "Running Lynis as requested...\n\n"
			if [ -f ./custom.prf ]; then
				sudo lynis audit system --profile custom.prf
			else
				sudo lynis audit system
			fi
		else
			pImportant "Recommend reviewing results and tweaking security to suit the specific needs of this machine"
		fi
		;;
	*) 
		pFail "Not proceeding any further"
		;;
esac

# give the following prompt a delimiter
printf "\n\n"
